import os
from setuptools import setup

version_py = os.path.join(os.path.dirname(__file__), 'fast5_collapser', 'version.py')
version = open(version_py).read().strip().split('=')[-1].replace('"','').strip()
print(version)
long_description = """
``fast5_collapser`` is a toolset for working fast5 files (HDF5 Structure)'
"""

HERE = os.path.dirname(__file__)

with open(os.path.join(HERE, "requirements.txt"), "r") as f:
    install_requires = [x.strip() for x in f.readlines()]

setup(
    name="fast5_collapser",
    version=version,
    install_requires=install_requires,
    requires=['python (>=3.6)'],
    packages=['fast5_collapser'],
    author="Martin Manolov ",
    description='A toolset for working fast5 files (HDF5 Structure)',
    long_description=long_description,
    url="https://gitlab.com/frason/fast5_collapser",
    package_dir={'fast5_collapser': "fast5_collapser"},
    package_data={'fast5_collapser': []},
    zip_safe=False,
    include_package_data=True,
    entry_points={
        'console_scripts': [
            'fast5_collapser=fast5_collapser.fast5_collapser_main:main',
        ],
    },
    author_email="martin.manolov@rwth-aachen.de",
    classifiers=[
        'Development Status :: 0 - Beta',
        'Intended Audience :: Science/Research',
        'License :: GNU General Public License (GPL)',
        'Topic :: Scientific/Engineering :: Bio-Informatics'
        ]
    )

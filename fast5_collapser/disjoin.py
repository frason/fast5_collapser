from .main_module import Fast5FileSet, Fast5File, get_progress_bar
import os

def run(parser, args):
	big_fast5 = Fast5FileSet(args.file)
	try:
	# Create target Directory
		os.mkdir(args.directory)
		print("Directory " , args.directory ,  " Created ")
	except FileExistsError:
		print("Directory " , args.directory ,  " already exists")
	os.chdir(args.directory)
	pbar = get_progress_bar(len(big_fast5.files))
	for group in big_fast5.files:
		big_fast5.copy_content_to_small_file(group)
		pbar.update(pbar.currval + 1)
	pbar.finish()
	big_fast5.close()

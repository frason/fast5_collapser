#!/usr/bin/env python3.6
import argparse

#logger
import logging
logger = logging.getLogger('fast5_collapser')

# fast5_collapser imports
import fast5_collapser.version

def run_subtool(parser, args):
	if args.command == 'collapse':
		from .collapse import run
	elif args.command == 'fasta':
		from .fasta import run
	elif args.command == 'fastq':
		from .fastq import run
	elif args.command == 'metadata':
		from .metadata import run
	elif args.command == 'disjoin':
		from .disjoin import run

	run(parser, args)


class ArgumentParserWithDefaults(argparse.ArgumentParser):
	def __init__(self, *args, **kwargs):
		super(ArgumentParserWithDefaults, self).__init__(*args, **kwargs)
		self.add_argument("-q", "--quiet", help="Do not output warnings to stderr",
		action="store_true",
		dest="quiet")

def main():
	logging.basicConfig()

	#########################################
	# create the top-level parser
	#########################################
	parser = argparse.ArgumentParser(prog='fast5_collapser', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
	parser.add_argument("-v", "--version", help="Installed fast5_collapser version",
		action="version",
		version="%(prog)s " + str(fast5_collapser.version.__version__))
	subparsers = parser.add_subparsers(title='[sub-commands]', dest='command', parser_class=ArgumentParserWithDefaults)

	#########################################
	# create the individual tool parsers
	#########################################

	##########
	# METADATA
	##########
	parser_metadata = subparsers.add_parser('metadata',
		help='Display metadata on the fast5 files inside the bigger file')
	parser_metadata.add_argument('file', metavar='FILE',
		help='The input FAST5 file.')
	parser_metadata.add_argument('--general', dest = 'general', default = False,
		action='store_true', help ='Display general information on Flowcell and Kit')
	parser_metadata.set_defaults(func=run_subtool)

	##########
	# COLLAPSE
	##########
	parser_collapse = subparsers.add_parser('collapse',
		help='Combine a set of FAST5 files in a single file')
	parser_collapse.add_argument('directory', metavar='INPUT DIRECTORY',
		help='The directory with the fast5 files')
	parser_collapse.add_argument('output', metavar='OUTPUT FILE',
		help='The name of the output for the set of FAST5 files.')
	parser_collapse.set_defaults(func=run_subtool)


	##########
	# DISJOIN
	##########
	parser_disjoin = subparsers.add_parser('disjoin',
		help='Disjoin a set of FAST5 files into a folder')
	parser_disjoin.add_argument('file', metavar='INPUT FILE',
		help='The name of the input file with the set of FAST5 files.')
	parser_disjoin.add_argument('directory', metavar='INPUT DIRECTORY',
		help='The directory to output fast5 files')
	parser_disjoin.set_defaults(func=run_subtool)




	##########
	# FASTQ
	##########
	parser_fastq = subparsers.add_parser('fastq',
		help='Extract FASTQ sequences from a set of FAST5 files')
	parser_fastq.add_argument('file', metavar='FILE',
		help='The input FAST5 file.')
	parser_fastq.set_defaults(func=run_subtool)


	##########
	# FASTA
	##########
	parser_fasta = subparsers.add_parser('fasta',
		help='Extract FASTA sequences from a set of FAST5 files')
	parser_fasta.add_argument('file', metavar='FILE',
		help='The input FAST5 file.')
	parser_fasta.set_defaults(func=run_subtool)

	#######################################################
	# parse the args and call the selected function
	#######################################################
	args = parser.parse_args()

	if args.quiet:
		logger.setLevel(logging.ERROR)

	try:
		args.func(parser, args)
	except IOError as e:
		if e.errno != 32:  # ignore SIGPIPE
			raise

if __name__ == "__main__":
	main()

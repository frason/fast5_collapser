from .main_module import Fast5FileSet

def run(parser, args):
	big_fast5 = Fast5FileSet(args.file)
	for group in big_fast5.files:
		big_fast5.get_fasta(group)
	for fa_string in big_fast5.fasta:
		print(fa_string)
	big_fast5.close()

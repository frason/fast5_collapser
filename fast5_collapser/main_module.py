import os
import glob
import h5py
from progressbar import RotatingMarker, ProgressBar, SimpleProgress, Bar, \
	Percentage, ETA

#logging
import logging
logger = logging.getLogger('fast5_collapser')



class Fast5DirHandler(object):

	patterns = ["*.fast5"]

	def __init__(self, dir):
		self.dir = dir
		self.files = []
		super(Fast5DirHandler, self).__init__()


		if os.path.isdir(self.dir):
			pattern = self.dir + os.path.sep + '*.fast5'
			files = glob.glob(pattern)
			self.files = files

	def __iter__(self):
		return self

	def next(self):
		if len(self.files) > 0:
			return self.files.pop(0)
		else:
			raise StopIteration()


class Fast5FileSet(object):

	def __init__(self, file):
		self.filename = file
		self.num_files_in_set = None
		self.is_open = self.open()
		self.fasta = []
		self.fastq = []
		self.metadata = []
		self.extract_fast5_files_list()

	def open(self):
		"""
		Open an ONT Fast5 file, assuming HDF5 format
		"""
		try:
			self.hdf5file = h5py.File(self.filename)
			return True
		except Exception as e:
			logger.warning("Cannot open file: %s. Perhaps it is corrupt? Moving on.\n" % (self.filename))
			return False


	def close(self):
		"""
		Close an open an ONT Fast5 file, assuming HDF5 format
		"""
		if self.is_open:
			self.hdf5file.close()
			self.is_open = False

	def get_num_files(self):
		"""
		Return the number of files in the FAST5 set.
		"""
		if self.num_files_in_set is None:
			self.num_files_in_set = len(list(self.hdf5file.keys()))
		return self.num_files_in_set

	def __iter__(self):
		return self

	def next(self):
		try:
			return Fast5File(self.files.next(), self.group)
		except Exception as e:
			raise StopIteration

	def extract_fast5_files_list(self):
		if '/keys' not in self.hdf5file:
			self.files = list(self.hdf5file.keys())
			ascii_keys = [key.encode('ascii', 'ignore') for key in self.files]
			self.hdf5file.create_dataset('keys', (len(ascii_keys), 1), 'S' + \
			 	str(max([len(string) for string in self.files])), ascii_keys)
		else:
			self.files = [key[0].decode('utf-8') for key in self.hdf5file["/keys"]]
		self.num_files_in_set = len(self.files)
		if not len(self.files):
			logger.warning("File is empty!")
		return None

	def get_fasta(self, group):
		#something
		fasta = self.extract_fastq_sequence(group)
		if fasta is not None:
			for item in fasta[0:2]:
				self.fasta.append(item)
			return None

	def get_fastq(self, group):
		fastq = self.extract_fastq_sequence(group)
		if fastq is not None:
			for item in fastq:
				self.fastq.append(item)
			return None

	def get_flowcell(self, group):
		try:
			return self.hdf5file[group + "/UniqueGlobalKey/context_tags"].attrs['flowcell_type'].decode('utf-8')
		except:
			pass

	def get_kit(self, group):
		try:
			return self.hdf5file[group + "/UniqueGlobalKey/context_tags"].attrs['sequencing_kit'].decode('utf-8')
		except:
			pass

	def get_project_name(self, group):
		try:
			return self.hdf5file[group + "/UniqueGlobalKey/context_tags"].attrs['user_filename_input'].decode('utf-8')
		except:
			pass

	def get_metadata(self, group):
		flowcell = self.get_flowcell(group)
		kit = self.get_kit(group)
		name = self.get_project_name(group)
		if not [flowcell, kit, name] in self.metadata:
			self.metadata.append([flowcell, kit, name])
		return None

	def extract_fastq_sequence(self, group):
		if group + "/Analyses/Basecall_1D_000/BaseCalled_template/Fastq" in self.hdf5file:
			fastq = self.hdf5file[group+"/Analyses/Basecall_1D_000/BaseCalled_template/Fastq"]
			fastq_splitted = fastq[()].	decode('utf-8').split('\n')
			fastq_splitted[0] = "@" + fastq_splitted[0].split()[-1] + ".fast5"
			return fastq_splitted
		else:
			return None


	def copy_content_to_small_file(self, small_fast5):
		"""
		Copies all information from the given fast5 group and creates a new file
		in the given folder, containing said information.
		"""
		new_file = Fast5File(small_fast5, write = True)
		for key in iter(self.hdf5file[small_fast5].keys()):
			self.hdf5file[small_fast5].copy(key, new_file.hdf5file)
		new_file.close()
		return None



def get_progress_bar(num_reads):
	bar_format = [RotatingMarker(), " ", SimpleProgress(), Bar(), Percentage(), " ", ETA()]
	return ProgressBar(maxval=num_reads, widgets=bar_format).start()


class Fast5File(object):

	def __init__(self, filename, write=False, multiprocessing=False):
		self.filename = filename
		self.is_open = self.open(write, multiprocessing)


		self.fasta = {}
		self.fastq = {}


	def __del__(self):
		self.close()

	def open(self, write, multiprocessing):
		"""
		Open an ONT Fast5 file, assuming HDF5 format
		"""
		mode = "r"
		if write:
			mode = 'w'

		driver = None
		if multiprocessing:
			driver = 'mpio'

		try:
			self.hdf5file = h5py.File(self.filename, mode, driver = driver)
			return True
		except Exception as e:
			logger.warning("Cannot open/create file: %s in mode %s. Perhaps it is corrupt? Moving on.\n" % (self.filename, mode))
			return False


	def close(self):
		"""
		Close an open an ONT Fast5 file, assuming HDF5 format
		"""
		if self.is_open:
			self.hdf5file.close()
			self.is_open = False


	def copy_content_to_big_file(self, big_fast5):
		"""
		Copies all information from the given fast5 file and creates a new group
		in the bigger file, containing said information.
		"""
		groupname = self.filename.split("/")[-1]
		big_fast5.hdf5file.create_group(groupname)
		for key in iter(self.hdf5file.keys()):
			self.hdf5file.copy(key, big_fast5.hdf5file[groupname+"/"])
		self.close()
		return None

	def create_keys_dataset(self, keys):
		"""
		Creates a dataset containing all the files stored, in case of many files
		and need of fast keys extraction
		"""
		ascii_keys = [key.split("/")[-1].encode('ascii', 'ignore') for key in keys]
		self.hdf5file.create_dataset('keys', (len(ascii_keys), 1), 'S' + \
		 	str(max([len(string) for string in keys])), ascii_keys)
		return None

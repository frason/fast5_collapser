from .main_module import Fast5DirHandler, Fast5File, get_progress_bar

def run(parser, args):
	fast5files =  Fast5DirHandler(dir=args.directory).files
	output_file = Fast5File(args.output, write = True, multiprocessing = False)

	pbar = get_progress_bar(len(fast5files))
	for filename in fast5files:
		Fast5File(filename).copy_content_to_big_file(output_file)
		pbar.update(pbar.currval + 1)
	pbar.finish()
	output_file.create_keys_dataset(fast5files)
	output_file.close()

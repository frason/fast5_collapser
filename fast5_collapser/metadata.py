from .main_module import Fast5FileSet

def run(parser, args):
	big_fast5 = Fast5FileSet(args.file)
	for group in big_fast5.files:
		big_fast5.get_metadata(group)
	if len(big_fast5.metadata) == 0:
		print("No meta information found")
	else:
		for metainfo in big_fast5.metadata:
			if None not in metainfo:
				print("Flowcell: " + metainfo[0] + "\nKit: " + \
					metainfo[1] + "\nProject name: " + metainfo[2] + "\n \n")

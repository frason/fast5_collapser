fast5_collapser: A tool to bring whole folder into a single fast5 file
Martin Manolov

Installation: 

    python3.6 setup.py install


Relies on the following python packages.

    h5py
    progressbar


Usage:

Collapse a folder into a single file

    fast5_collapser collapse [-h] [-q] INPUT DIRECTORY OUTPUT FILE

Extract fasta and or fastq

	fast5_collapser fasta [-h] [-q] FILE
	fast5_collapser fastq [-h] [-q] FILE
	fast5_collapser metadata [-h] [-q] FILE

Fuller documentation will be available soon.
